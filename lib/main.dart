import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        ExtractArgumentScreen.routeName: (context) =>
            const ExtractArgumentScreen(),
      },
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text('Navigate to Screen that extracts arguments'),
          onPressed: () {
            Navigator.pushNamed(context, ExtractArgumentScreen.routeName,
                arguments: ScreenArguments('Extract Argument Screen',
                    'This message is extracted in the build method'));
          },
        ),
      ),
    );
  }
}

class ScreenArguments {
  final String titles;
  final String message;

  ScreenArguments(this.titles, this.message);
}

class ExtractArgumentScreen extends StatelessWidget {
  const ExtractArgumentScreen({Key? key}) : super(key: key);
  static const routeName = '/extractArgumets';
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(args.titles),
      ),
      body: Center(
        child: Text(args.message),
      ),
    );
  }
}
